package main

import (
	"log"
	"os"
	apiApp "wbbot/internal/api"
)

func main() {
	api, err := apiApp.AppInit("api", os.Args[1:])
	if err != nil {
		log.Fatalf("fail api init: %s", err)
	}

	api.Start()
}
