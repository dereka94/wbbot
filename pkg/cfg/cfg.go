package cfg

import (
	"github.com/spf13/viper"
)

type Config struct {
	cfg    *viper.Viper
	prefix string
}

func CfgInit(path string) (*Config, error) {
	cfg := &Config{}

	cfg.cfg = viper.New()
	cfg.cfg.SetConfigName("api")
	cfg.cfg.SetConfigType("yaml")
	cfg.cfg.AddConfigPath(path)

	err := cfg.cfg.ReadInConfig()
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

func (c *Config) GetPrefixCfg(prefix string) *Config {
	cfg := &Config{}
	cfg.cfg = c.cfg
	cfg.prefix = prefix

	return cfg
}

func (c *Config) GetString(name string) string {
	return c.cfg.GetString(c.prefix + "." + name)
}

func (c *Config) GetInt(name string) int {
	return c.cfg.GetInt(c.prefix + "." + name)
}

func (c *Config) GetBool(name string) bool {
	return c.cfg.GetBool(c.prefix + "." + name)
}
