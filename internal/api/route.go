package apiApp

import (
	"context"
	"fmt"
	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"os"
	"os/signal"
	"wbbot/pkg/cfg"
)

type route struct {
	tgbot *bot.Bot
}

func routeInit(cfg *cfg.Config) (route, error) {
	route := route{}

	b, err := bot.New(cfg.GetString("token"))
	if err != nil {
		return route, err
	}
	b.RegisterHandler(bot.HandlerTypeMessageText, "/start", bot.MatchTypeExact, myStartHandler)

	route.tgbot = b

	return route, nil
}

func (r route) Start() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	r.tgbot.Start(ctx)
}

func myStartHandler(ctx context.Context, b *bot.Bot, update *models.Update) {
	ok, err := b.SetMyCommands(ctx, &bot.SetMyCommandsParams{
		Commands: []models.BotCommand{
			{Command: "6", Description: "11"},
			{Command: "7", Description: "22"},
		},
		Scope:        nil,
		LanguageCode: "",
	})
	fmt.Println(ok)
	if err != nil {
		fmt.Println(err.Error())
	}
	ok, err = b.SetChatMenuButton(ctx, &bot.SetChatMenuButtonParams{
		ChatID:     update.Message.Chat.ID,
		MenuButton: models.MenuButtonDefault{Type: "default"},
	})
	fmt.Println(ok)
	if err != nil {
		fmt.Println(err.Error())
	}
}
