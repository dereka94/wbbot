package apiApp

import (
	"fmt"
	"wbbot/pkg/cfg"
)

type Api struct {
	name       string
	cfgPath    string
	Cfg        *cfg.Config
	Route      Route
	UserClient *UserClient
}

type Route interface {
	Start()
}

type UserClient interface {
	Registration(name string, pwd string) (*User, error)
	Login(name string, pwd string) (*User, error)
	Logout(name string, pwd string) error
}

type User struct {
	Name string
	ID   int64
}

func AppInit(name string, agrs []string) (*Api, error) {
	app := &Api{}

	app.parseCmdArgs(agrs)

	cfg, err := cfg.CfgInit(app.cfgPath)
	if err != nil {
		return nil, fmt.Errorf("app init fail read config: %w", err)
	}
	app.Cfg = cfg

	route, err := routeInit(cfg.GetPrefixCfg("route"))
	if err != nil {
		return nil, fmt.Errorf("app init fail init route: %s", err)
	}
	app.Route = route

	app.name = name

	return app, nil
}

func (a *Api) parseCmdArgs(args []string) error {
	a.cfgPath = "../../config/api.yaml"

	i := 0
	for i < len(args) {
		switch args[i] {
		case "-c", "--config":
			{
				if args[i+1] == "" {
					return fmt.Errorf("bad args %d", i/2)
				}
				a.cfgPath = args[i+1]
			}

		}
		i = i + 1
	}
	return nil
}

func (a *Api) Start() {
	a.Route.Start()
}
